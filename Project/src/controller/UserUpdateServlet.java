package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userdao = new UserDao();
		User user = userdao.findByID(id);
		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("User", user);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//入力データを受け取る
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String rePassword = request.getParameter("rePassword");
		String password = request.getParameter("password");
		String birth_date = request.getParameter("birth_date");
		String loginId = request.getParameter("loginId");

		if (name.equals("") || birth_date.equals("") || !(password.equals(rePassword))) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			UserDao userdao = new UserDao();
			User user = userdao.findByID(id);
			// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
			request.setAttribute("User", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
			dispatcher.forward(request, response);
			return;

		}

		UserDao userDao = new UserDao();
		if (password.equals("") && rePassword.equals("")) {
			userDao.updatePass(name, birth_date, loginId);
		}

		else {

			userDao.update(name, password, birth_date, loginId);
		}

		response.sendRedirect("UserListServlet");

	}

}
