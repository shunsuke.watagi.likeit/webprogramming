<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<header>
		<h1 class="headline">
			<a>${User.name}さん</a>
		</h1>
		<a class="headline1" href="LogoutServlet">ログアウト</a>
		<ul class="nav-list">

		</ul>
	</header>
	<h1 class="center">ユーザ情報詳細参照</h1>
	<div class="form-group row">
		<label for="staticEmail" class="col-sm-2 col-form-label">ログインID:
			${User.loginId}</label>
		<div class="col-sm-10"></div>
	</div>
	<div class="form-group row">
		<label for="staticEmail" class="col-sm-2 col-form-label">ユーザ名:
			${User.name}</label>
		<div class="col-sm-10"></div>
	</div>
	<div class="form-group row">
		<label for="staticEmail" class="col-sm-2 col-form-label">生年月日:
			${User.birthDate}</label>
		<div class="col-sm-10"></div>
	</div>
	<div class="form-group row">
		<label for="staticEmail" class="col-sm-2 col-form-label">登録日時:
			${User.createDate}</label>
		<div class="col-sm-10"></div>
	</div>
	<div class="form-group row">
		<label for="staticEmail" class="col-sm-2 col-form-label">更新日時:${User.updateDate}</label>
		<div class="col-sm-10"></div>
	</div>
	<a class="nav-link" href="UserListServlet">戻る</a>
</body>
</html>
