<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<header>
		<h1 class="headline">
			<a>${userInfo.name}さん</a>
		</h1>
		<a class="headline1" href="LogoutServlet">ログアウト</a>
		<ul class="nav-list">

		</ul>
	</header>
	<form action="UserListServlet" method="post">
		<h1 class="center">ユーザ一覧</h1>
		<div class="right">
			<a class="nav-link" href="UserRegistServlet">新規登録</a>
		</div>
		<div class="form-group ">
			<label for="inputLogin_id" class="col-sm-2 col-form-label">ログインID</label>
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="inputLogin_id"
				name="loginId">
		</div>




		<div class="form-group ">
			<label for="inputLogin_id" class="col-sm-2 col-form-label">ユーザ名</label>
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="inputLogin_id"
				name="name">
		</div>




		<div class="form-group ">
			<label for="inputLogin_id" class="col-sm-2 col-form-label">生年月日</label>
		</div>


		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-9 row">
					<input type="date" class="form-control col-sm-4"
						placeholder="年/月/日" name="birthDate"> ～ <input type="date"
						class="form-control col-sm-4" placeholder="年/月/日"
						name="birthDate2">
				</div>
			</div>
		</div>
		<div class="right">
			<button type="submit" class="btn btn-primary">検索</button>
		</div>

	</form>

	<table border="1">
		<tr>
			<th>ログインID</th>
			<th>ユーザ名</th>
			<th>生年月日</th>
			<th></th>
		</tr>
		<c:forEach var="user" items="${userList}">
			<tr>
				<c:if test="${user.id!=1}">
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<!-- TODO 未実装；ログインボタンの表示制御を行う -->
					<td><c:if test="${userInfo.id==1}">

							<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
							<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
							<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
						</c:if> <c:if test="${userInfo.id!=1}">
							<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
							<c:if test="${userInfo.id==user.id}">
								<a class="btn btn-success"
									href="UserUpdateServlet?id=${user.id}">更新</a>
							</c:if>
						</c:if></td>
				</c:if>
			</tr>

		</c:forEach>

		<tr>
			<td></td>
			<td></td>
		</tr>
	</table>


</body>









</html>
