<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <link href="style.css" rel="stylesheet" type="text/css" />
</head>
	<body>

        <header>
             <a class="headline1" href="Logoutservlet">ログアウト</a>
  	<h1 class="headline">
      <a>${User.name}さん</a>
    </h1>


  </header>
  <form action="UserRegistServlet" method="post">
		<h1 class="center">ユーザ新規登録</h1>

		<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	

         <div class="form-group ">
    <label for="inputLogin_id" class="col-sm-2 col-form-label">ログインID</label></div>
    <div class="col-sm-12">
      <input type="text" class="form-control" id="inputLoginId" name="loginId">
    </div>
             <div class="form-group ">
    <label for="inputLogin_id" class="col-sm-2 col-form-label">パスワード</label></div>
    <div class="col-sm-12">
      <input type="password" class="form-control" id="inputPassword" name="password" >
    </div><div class="form-group ">
    <label for="inputLogin_id" class="col-sm-2 col-form-label">パスワード(確認)</label></div>
    <div class="col-sm-12">
      <input type="password" class="form-control" id="inputRePassword" name="rePassword">
    </div>
    <div class="form-group ">
    <label for="inputLogin_id" class="col-sm-2 col-form-label">ユーザ名</label></div>
    <div class="col-sm-12">
      <input type="text" class="form-control" id="inputName" name="name">
    </div>
    <div class="form-group ">
    <label for="inputLogin_id" class="col-sm-2 col-form-label">生年月日</label></div>
    <div class="col-sm-12">
      <input type="date" class="form-control" id="inputBirth_date" name="birth_date">
    </div>
     <div class="center">
  <button type="submit" class="btn btn-primary col-sm-1">登録</button>
                 </div>
     <a class="nav-link" href="UserListServlet">戻る</a>

</form>
	</body>
</html>
