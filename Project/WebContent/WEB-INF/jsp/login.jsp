<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>
	<body>

        <h1 class="center">ログイン画面</h1>
<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<form class="form-horizontal"action="LoginServlet" method="post">
  <div class="form-group ">
    <label for="inputLogin_id" class="col-sm-2 col-form-label">ログインID</label></div>
    <div class="col-sm-12">

      <input type="text" class="form-control" id="inputLoginId" placeholder="ログインID"name="loginId">
    </div>
<div class="form-group ">
    <label for="inputPassword" class="col-sm-2 col-form-label">パスワード</label>
    <div class="col-sm-12">
      <input type="password" class="form-control" id="inputPassword" placeholder="パスワード"name="password">
    </div>
    </div>
 <div class="center">
  <button  type="submit" class="btn btn-primary" >ログイン</button>
      </div>
     </form>
	</body>
</html>
