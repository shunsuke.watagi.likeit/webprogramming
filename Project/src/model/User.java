package model;

import java.io.Serializable;
import java.util.Date;

/**
 * Userテーブルのデータを格納するためのBeans
 * @author takano
 *
 */
public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private String createDate;
	private String updateDate;

	// 一覧検索用
	public User(String loginId, String name, Date birthDate) {
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
	}

	// 全てのデータをセットするコンストラクタ
	public User(int id, String loginId, String name, Date birthDate, String password, String createDate,
			String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;

	}

	public User(int id, String loginId, String name, Date birthDate, String createDate,
			String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;

	}

	//ログインセッションを保存するためのコンストラクタ
	public User(String loginId, String password) {
		this.loginId = loginId;
		this.password=password;
	}

	public User(int idData, String loginIdData, String nameData) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = idData;
		this.loginId = loginIdData;
		this.name = nameData;
	}

	//詳細表示
	public User(String loginIdData, String nameData, java.sql.Date birthDateData, String createdateData,
			String updateData) {
		this.loginId = loginIdData;
		this.name = nameData;
		this.birthDate = birthDateData;
		this.createDate = createdateData;
		this.updateDate = updateData;
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

}
